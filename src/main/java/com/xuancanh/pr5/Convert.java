package com.xuancanh.pr5;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

/**
 * @author Xuan Canh
 * @date 2021-05-28 3:10
 */
public class Convert {
    private int value;
    private String from;
    private String to;

    private boolean check;

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public Convert(int value, String from, String to) {
        this.check = true;
        this.value = value;
        this.from = from;
        this.to = to;
    }

    public Convert(String value, String from, String to) {
        this.check = false;
        this.from = from;
        this.to = to;
    }

    public Convert() {

    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    @Override
    public String toString() {
        return "Convert{" +
                "value=" + value +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                '}';
    }

    public static String fmt(double d)
    {
        if(d == (long) d)
            return String.format("%d",(long)d);
        else
            return String.format("%s",d);
    }

    public void writeFile(String path, String output) {
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {

            File file = new File(path);

            if (!file.exists()) {
                file.createNewFile();
            }

            fw = new FileWriter(file.getAbsoluteFile(), true);
            bw = new BufferedWriter(fw);
            bw.write(output);
            System.out.println("Success...");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null)
                    bw.close();
                if (fw != null)
                    fw.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public String conv() {
        String[] unitOfLength = {"mm", "cm", "dm", "m", "km"};
        double valueTemp = 0;

        if(!check) {
            return  "Value is a number, please try again.";
        }
        else if (!Arrays.asList(unitOfLength).contains(this.from) || !Arrays.asList(unitOfLength).contains(this.to)) {
            return "Unit must be one of(mm, cm, dm, m, km), please try again.";
        } else {
            if (from.equals("mm")) {
                if (to.equals("mm"))
                    valueTemp = value;
                else if (to.equals("cm"))
                    valueTemp = (double) value / 10;
                else if (to.equals("dm"))
                    valueTemp = (double) value / 100;
                else if (to.equals("m"))
                    valueTemp = (double) value / 1000;
                else if (to.equals("km"))
                    valueTemp = (double) value / 1000000;
            } else if (from.equals("cm")) {
                if (to.equals("mm"))
                    valueTemp = value * 10;
                else if (to.equals("cm"))
                    valueTemp = value;
                else if (to.equals("dm"))
                    valueTemp = (double) value / 10;
                else if (to.equals("m"))
                    valueTemp = (double) value / 100;
                else if (to.equals("km"))
                    valueTemp = (double) value / 100000;
            } else if (from.equals("dm")) {
                if (to.equals("mm"))
                    valueTemp = value * 100;
                else if (to.equals("cm"))
                    valueTemp = value * 10;
                else if (to.equals("dm"))
                    valueTemp = value;
                else if (to.equals("m"))
                    valueTemp = (double) value / 10;
                else if (to.equals("km"))
                    valueTemp = (double) value / 10000;
            } else if (from.equals("m")) {
                if (to.equals("mm"))
                    valueTemp = value * 1000;
                else if (to.equals("cm"))
                    valueTemp = value * 100;
                else if (to.equals("dm"))
                    valueTemp = value * 10;
                else if (to.equals("m"))
                    valueTemp = value;
                else if (to.equals("km"))
                    valueTemp = (double) value / 1000;
            } else if (from.equals("km")) {
                if (to.equals("mm"))
                    valueTemp = value * 1000000;
                else if (to.equals("cm"))
                    valueTemp = value * 100000;
                else if (to.equals("dm"))
                    valueTemp = value * 10000;
                else if (to.equals("m"))
                    valueTemp = value * 1000;
                else if (to.equals("km"))
                    valueTemp = value;
            }

            return value + " " + from + " = " + fmt(valueTemp) + " " + to;
        }
    }
}
