package com.xuancanh.pr5;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class Pr5ApplicationTests {


    //From m
    @Test
    public void testCase1() {
        Convert convert = new Convert(1, "m", "km");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("1 m = 0.001 km");
    }

    @Test
    public void testCase2() {
        Convert convert = new Convert(1, "m", "m");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("1 m = 1 m");
    }


    @Test
    public void testCase3() {
        Convert convert = new Convert(1, "m", "dm");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("1 m = 10 dm");
    }

    @Test
    public void testCase4() {
        Convert convert = new Convert(1, "m", "cm");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("1 m = 100 cm");
    }

    @Test
    public void testCase5() {
        Convert convert = new Convert(1, "m", "mm");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("1 m = 1000 mm");
    }

    //From km
    @Test
    public void testCase6() {
        Convert convert = new Convert(1, "km", "km");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("1 km = 1 km");

    }

    @Test
    public void testCase7() {
        Convert convert = new Convert(1, "km", "m");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("1 km = 1000 m");

    }

    @Test
    public void testCase8() {
        Convert convert = new Convert(1, "km", "dm");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("1 km = 10000 dm");
    }

    @Test
    public void testCase9() {
        Convert convert = new Convert(1, "km", "cm");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("1 km = 100000 cm");
    }

    @Test
    public void testCase10() {
        Convert convert = new Convert(1, "km", "mm");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("1 km = 1000000 mm");
    }

    //From dm
    @Test
    public void testCase11() {
        Convert convert = new Convert(1, "dm", "km");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("1 dm = 1.0E-4 km");
    }

    @Test
    public void testCase12() {
        Convert convert = new Convert(1, "dm", "m");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("1 dm = 0.1 m");
    }

    @Test
    public void testCase13() {
        Convert convert = new Convert(1, "dm", "dm");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("1 dm = 1 dm");
    }

    @Test
    public void testCase14() {
        Convert convert = new Convert(1, "dm", "cm");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("1 dm = 10 cm");
    }

    @Test
    public void testCase15() {
        Convert convert = new Convert(1, "dm", "mm");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("1 dm = 100 mm");
    }

    //From cm
    @Test
    public void testCase16() {
        Convert convert = new Convert(1, "cm", "km");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("1 cm = 1.0E-5 km");
    }

    @Test
    public void testCase17() {
        Convert convert = new Convert(1, "cm", "m");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("1 cm = 0.01 m");
    }

    @Test
    public void testCase18() {
        Convert convert = new Convert(1, "cm", "dm");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("1 cm = 0.1 dm");
    }

    @Test
    public void testCase19() {
        Convert convert = new Convert(1, "cm", "cm");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("1 cm = 1 cm");
    }

    @Test
    public void testCase20() {
        Convert convert = new Convert(1, "cm", "mm");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("1 cm = 10 mm");
    }

    //From mm
    @Test
    public void testCase21() {
        Convert convert = new Convert(1, "mm", "km");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("1 mm = 1.0E-6 km");
    }

    @Test
    public void testCase22() {
        Convert convert = new Convert(1, "mm", "m");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("1 mm = 0.001 m");
    }

    @Test
    public void testCase23() {
        Convert convert = new Convert(1, "mm", "dm");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("1 mm = 0.01 dm");
    }

    @Test
    public void testCase24() {
        Convert convert = new Convert(1, "mm", "cm");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("1 mm = 0.1 cm");
    }

    @Test
    public void testCase25() {
        Convert convert = new Convert(1, "mm", "mm");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("1 mm = 1 mm");
    }

    @Test
    public void testCase26() {
        Convert convert = new Convert(1, "em", "mm");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("Unit must be one of(mm, cm, dm, m, km), please try again.");
    }

    @Test
    public void testCase27() {
        Convert convert = new Convert(1, "mm", "lm");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("Unit must be one of(mm, cm, dm, m, km), please try again.");
    }

    @Test
    public void testCase28() {
        Convert convert = new Convert("pr5", "m", "mm");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("Value is a number, please try again.");
    }

    @Test
    public void testCase29() {
        Convert convert = new Convert(1, "qm", "my");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("Unit must be one of(mm, cm, dm, m, km), please try again.");
    }

    @Test
    public void testCase30() {
        Convert convert = new Convert("pr5", "qm", "my");
        String output = convert.conv();
        convert.writeFile("result.txt", output + "\n");
        assertThat(output).isEqualTo("Value is a number, please try again.");
    }
}